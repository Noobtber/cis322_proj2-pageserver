from flask import Flask, send_from_directory, abort, make_response, render_template
import os.path

app = Flask(__name__, template_folder = "pages/errors/")


# Default route for home
@app.route("/")
def hello():
    return "UOCIS docker demo!"

# Default route for requested files
@app.route("/<path:filename>")
def dynamicPath(filename):

    # 403 Logic
    if ".." in filename or "~" in filename or "//" in filename:
        abort(403)
    return make_response(send_from_directory("pages", filename), 200)

# 404 Handler
@app.errorhandler(404)
def error_404(error):
    return make_response(render_template("404.html"), 404)

# 403 Handler
@app.errorhandler(403)
def error_403(error):
    return make_response(render_template("403.html"), 403)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
